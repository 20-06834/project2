import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  
  {
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full'
  },
  {
    path: 'splash',
    loadChildren: () => import('./splash/splash.module').then( m => m.SplashPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'button1',
    loadChildren: () => import('./button1/button1.module').then( m => m.Button1PageModule)
  },
  {
    path: 'button2',
    loadChildren: () => import('./button2/button2.module').then( m => m.Button2PageModule)
  },
  {
    path: 'button3',
    loadChildren: () => import('./button3/button3.module').then( m => m.Button3PageModule)
  },
  {
    path: 'b1.page1',
    loadChildren: () => import('./b1.page1/b1.page1.module').then( m => m.B1Page1PageModule)
  },
  {
    path: 'b1.page2',
    loadChildren: () => import('./b1.page2/b1.page2.module').then( m => m.B1Page2PageModule)
  },
  {
    path: 'b1.page3',
    loadChildren: () => import('./b1.page3/b1.page3.module').then( m => m.B1Page3PageModule)
  },
  {
    path: 'b2.page1',
    loadChildren: () => import('./b2.page1/b2.page1.module').then( m => m.B2Page1PageModule)
  },
  {
    path: 'b2.page2',
    loadChildren: () => import('./b2.page2/b2.page2.module').then( m => m.B2Page2PageModule)
  },
  {
    path: 'b2.page3',
    loadChildren: () => import('./b2.page3/b2.page3.module').then( m => m.B2Page3PageModule)
  },
  {
    path: 'b2.page4',
    loadChildren: () => import('./b2.page4/b2.page4.module').then( m => m.B2Page4PageModule)
  },
  {
    path: 'b2.page5',
    loadChildren: () => import('./b2.page5/b2.page5.module').then( m => m.B2Page5PageModule)
  },
  {
    path: 'b2.page6',
    loadChildren: () => import('./b2.page6/b2.page6.module').then( m => m.B2Page6PageModule)
  },
  {
    path: 'b2.page7',
    loadChildren: () => import('./b2.page7/b2.page7.module').then( m => m.B2Page7PageModule)
  },
  {
    path: 'b2.page8',
    loadChildren: () => import('./b2.page8/b2.page8.module').then( m => m.B2Page8PageModule)
  },
  {
    path: 'b2.page9',
    loadChildren: () => import('./b2.page9/b2.page9.module').then( m => m.B2Page9PageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

